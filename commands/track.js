const {
  // UpsClient,
  // UpsMiClient,
  // UspsClient,
  // DhlClient,
  // FedexClient,
  // LasershipClient,
  // AmazonClient,
  guessCarrier
} = require('shipit')

const carriers = [
  'ups',
  'ups-mi',
  'usps',
  'dhl',
  'fedex',
  'lasership',
  'amazon'
]

module.exports = {
  name: 'track',
  description: 'Track a package',
  execute (message) {
    const args = message.content.split(' ')
    args.shift()
    const channel = message.channel

    if (args.length === 0) {
      channel.send('Status of your packages')
    } else if (carriers.includes(args[0])) {
      channel.send('Supported carrier supplied')
    } else {
      const possibleCarriers = guessCarrier(args[0])
      if (possibleCarriers.length === 0) {
        channel.send('Unrecognized tracking number format')
      } else {
        const tryCarrier = possibleCarriers[0]
        let foundCarrier = false
        while (foundCarrier === false) {
          if (carriers.includes(tryCarrier)) {
            foundCarrier = tryCarrier
          }
        }

        if (foundCarrier === false) {
          channel.send('Carrier not supported')
        } else {
          channel.send(`Tracking ${foundCarrier} package`)
        }
      }
    }
  }
}
